<?php
class Publication
{
    protected $id = 0;
    protected $heading = '';
    protected $entrance_text = '';
    protected $type = '';
    protected $full_text = '';

    public function __construct($heading , $entrance_text, $type , $full_text)
    {
        $this->heading = $heading;
        $this->entrance_text = $entrance_text;
        $this->type = $type;
        $this->full_text = $full_text;
    }

    static public function getPublicationById($id, PDO $pdo)
    {
        $sql = 'SELECT * FROM notes WHERE id=:id';
        $pdoSt = $pdo->prepare($sql);
        $pdoSt->bindValue(':id', $id);
        $pdoSt->execute();
        $notes = $pdoSt->fetch();
        $note = new self($notes['heading'], $notes['entrance_text'],  $notes['type'] , $notes['full_text'] );
        $note->setId($notes['id']);
        return $note;
    }

    public function setId($id){
        $this->id = $id;
    }


}