<?php
if(!isset($_GET['id'])){
    header('Location:index.php');
}
require_once 'db_connection.php';
require_once 'Publication.Class.php';
require_once 'News.Class.php';
require_once 'Article.Class.php';
require_once 'PublicationsWriter.Class.php';

try{
    $id = (int)$_GET['id'];
    $sql = "SELECT * FROM notes WHERE id = :id";
    $x = $pdo->prepare($sql);
    $x->bindValue('id', $id);
    $x->execute();
    $note = $x->fetch();
    $type = $note['type'];

    echo '<h1>'.$note['heading']. '</h1>'.'<br>' .'<br>' .'<h4>'. $note['entrance_text']. '</h4>' .'<br>'.$note['full_text']. '<br>' ;
    if ($type == 'news'){
        echo 'Источник:'. $note['source'] ;
    }
    elseif ($type == 'article'){
        echo 'Автор:'. $note['author'] ;
    }

}catch (Exception $e){
    echo 'Error' . $e->getMessage();
    die();
}


