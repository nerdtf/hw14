<?php
class PublicationsWriter
{
    protected $publications = [];
    public function __construct($type , PDO $pdo)
    {
        $i = 0;
        $sql = 'SELECT * FROM notes WHERE type=:type';
        $pdoSt = $pdo->prepare($sql);
        $pdoSt->bindValue(':type', $type);
        $pdoSt->execute();
        $notes = $pdoSt->fetchAll();

        if ($type == 'news') {

            foreach ($notes as $note) {
                $arr[] = new News($note['heading'], $note['entrance_text'], $note['type'], $note['full_text'], $note['source']);
                $arr[$i]->setId($note['id']);
                $i++;
            }
            $this->publications = $arr;
        }
        elseif ($type == 'article'){
            foreach ($notes as $note) {
                $arr[] = new Article($note['heading'], $note['entrance_text'], $note['type'], $note['full_text'], $note['author']);
                $arr[$i]->setId($note['id']);
                $i++;
            }
            $this->publications = $arr;
        }


    }

     public function writer($type , PDO $pdo){
        $sql = 'SELECT * FROM notes WHERE type=:type';
        $pdoSt = $pdo->prepare($sql);
        $pdoSt->bindValue(':type', $type);
        $pdoSt->execute();
        $notes = $pdoSt->fetchAll();

        foreach($notes as $note){

            $str='<h2>'.$note['heading']. '</h2>' . $note['entrance_text'] .'<br>'. '<a href="page2.php?id='.$note['id'].'">'.'Читать полностью'.'</a>'.'<br>' ;
            echo $str;

        }


    }



}