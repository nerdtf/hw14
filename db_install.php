<meta charset="utf-8">
<?php
require_once 'db_connection.php';
try{
	$sql = 'CREATE TABLE notes(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		heading TEXT NOT NULL,		
		entrance_text TEXT NOT NULL,
		type TEXT NOT NULL,
		full_text TEXT NOT NULL,
		author TEXT, 
		source TEXT 
	)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
	$pdo->exec($sql);

}catch(Exception $e){
	echo "Не удалось создать таблицу" . $e->getMessage();
}