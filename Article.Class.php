<?php
class Article extends Publication
{
    protected $author = '';

    public function __construct($heading , $entrance_text, $type , $full_text , $author){
        parent::__construct($heading , $entrance_text, $type , $full_text);
        $this->author=$author;
    }
    static public function getPublicationById($id, PDO $pdo){
        $sql = 'SELECT * FROM notes WHERE id=:id';
        $pdoSt = $pdo->prepare($sql);
        $pdoSt->bindValue(':id', $id);
        $pdoSt->execute();
        $notes = $pdoSt->fetch();
        $note = new self($notes['heading'], $notes['entrance_text'],  $notes['type'] , $notes['full_text'] , $notes['author']);
        $note->setId($notes['id']);
        return $note;
    }



}