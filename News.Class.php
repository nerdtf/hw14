<?php


class News extends Publication
{
    protected $source = "";

    public function __construct($heading , $entrance_text, $type , $full_text , $source)
    {
        parent::__construct($heading , $entrance_text, $type , $full_text);
        $this->source = $source;
    }

    static public function getPublicationById($id, PDO $pdo){
        $sql = 'SELECT * FROM notes WHERE id=:id';
        $pdoSt = $pdo->prepare($sql);
        $pdoSt->bindValue(':id', $id);
        $pdoSt->execute();
        $notes = $pdoSt->fetch();
        $note = new self($notes['heading'], $notes['entrance_text'],  $notes['type'] , $notes['full_text'] , $notes['source']);
        $note->setId($notes['id']);
        return $note;
    }



}