<meta charset="utf-8">
<?php
try{
	$pdo = new PDO('mysql:host=localhost;dbname=hw14',
	'hw14_user', '123456789');
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo->exec('SET NAMES "utf8"');

}catch(Exception $e){
	echo "Не удалось подключится к бд!". $e->getMessage();
	die();
}